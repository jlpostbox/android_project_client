package com.tool;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

public class ImageLoader {

	private Activity a = null;
	private Context context = null;


	public ImageLoader(Activity activity, Context context) {
		this.a = activity;
		this.context = context;
	}

	public void getImage(String imageName , ImageView imageView) {

		DisplayImageThread displayImageThread = new DisplayImageThread(imageName , imageView);
		Thread thread = new Thread(displayImageThread);
		thread.start();
		
		
	}

	/**
	 * 新线程，用于下载图片
	 * @author Administrator
	 *
	 */
	class DisplayImageThread implements Runnable {
		private String imageName;
		private ImageView imageView;
		private SetImageThread setImageThread;

		public DisplayImageThread(String imageName , ImageView i) {
			this.imageName = imageName;
			this.imageView = i;
			setImageThread = new SetImageThread(imageName,i);
		}

		public void run() {
			
			HttpDownloader httpDownloader = new HttpDownloader();
			int result = httpDownloader.downFile(context , imageName);
			if(result == -1){ //下载失败
			}
			else if(result == 0){ //成功下载
				a.runOnUiThread(setImageThread);
			}
			else if(result == 1){  //文件已存在
				a.runOnUiThread(setImageThread);
			}

		}
	}

	/**
	 * 运行在UI上的线程，用于填充ImageView
	 * @author Administrator
	 *
	 */
	class SetImageThread implements Runnable{
		
		private ImageView imageView = null;
		private String localImagePath = null;
		private File f = null;
		private Bitmap bm = null;
		
		public SetImageThread(String imageName , ImageView imageView){
			localImagePath = Utils.getTemporaryFile(context) + "/" +imageName;
			this.imageView = imageView;
		}
		public void run(){
			f = new File(localImagePath);
			bm = Utils.decodeFile(f);
			imageView.setImageBitmap(bm);
		}
	}

}
