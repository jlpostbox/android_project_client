package com.tool;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;

public class FileUtils {
	private File SDCardRoot;

	/**
	 * 获取存在图片的目录
	 */
	public FileUtils(Context c) {
		SDCardRoot = Utils.getTemporaryFile(c);
	}

	/**
	 * 创建文件
	 * 
	 * @throws IOException
	 */
	public File createFileInSDCard(String fileName)
			throws IOException {
		File file = new File(SDCardRoot , fileName);
		file.createNewFile();
		return file;
	}

	/**
	 * 创建目录
	 * 
	 * @param dirName
	 */
	/*public File creatSDDir(String dir) {
		File dirFile = new File(SDCardRoot + dir);
		return dirFile;
	}*/

	/**
	 * 检查文件是否存在
	 */
	public boolean isFileExist(String fileName) {
		File file = new File(SDCardRoot , fileName);
		return file.exists();
	}

	/**
	 * 向SDCard写InputStream
	 */
	public File write2SDFromInput(String fileName,
			InputStream input) {

		File file = null;
		OutputStream output = null;
		try {
			//creatSDDir(path);
			file = createFileInSDCard(fileName);
			output = new FileOutputStream(file);
			byte buffer[] = new byte[4 * 1024];
			int temp;
			while ((temp = input.read(buffer)) != -1) {
				output.write(buffer, 0, temp);
			}
			output.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return file;
	}

}