package com.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class Utils {
	
	//public static String SERVER_URL = "http://192.168.3.33:8080/AndroidServer/";
	public static String SERVER_URL = "http://192.168.101.117:8080/AndroidServer/";
	public static String DOWN_FILE_PATH = "UploadImages/";
	
	public static boolean debugFlag = true;
	public static String nLine = System.getProperty("line.separator");
	
	public static final String MENU_DEFAULT = "default";
	public static final String MENU_SEARCH = "search";
	public static final String MENU_TOP = "top";
	public static final String MENU_CATEGORY = "category";
	public static final String MENU_FILTERBYSIGNID = "filterBySignId";

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static String getDisplayDateStr(String dateStr) {
		// dateStr = "Thu Apr 30 01:33:41 +0000 2009";
		Date dd = new Date(dateStr);
		SimpleDateFormat myFmt = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		String str = "";
		try {
			str = myFmt.format(dd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		dout(str);
		return str;
	}

	public static String getDateStr(String dateStr) {
		Date date = str2Date(dateStr);
		return getHowLongStr(date);
	}

	/** 输入"Thu Apr 30 01:33:41 +0000 2009" 返回Date */
	public static Date str2Date(String dateStr) {
		return new Date(dateStr);
	}

	/** 传入丄1�7个Date，判断是多久前，返回示例"4小时剄1�7" */
	public static String getHowLongStr(Date date) {
		String rs = "";
		Long i = date.getTime();
		Date now = new Date();
		Long j = now.getTime();
		dout(j - i);
		long day = 1000 * 60 * 60 * 24;
		long hour = 1000 * 60 * 60;
		long min = 1000 * 60;
		long sec = 1000;
		if (((j - i) / day) > 0)
			rs = ((j - i) / day) + "天前";
		else if (((j - i) / hour) > 0)
			rs = ((j - i) / hour) + "小时剄1�7";
		else if (((j - i) / min) > 0)
			rs = ((j - i) / min) + "分钟剄1�7";
		else if (((j - i) / sec) > 0)
			rs = ((j - i) / sec) + "秒前";
		return rs;
	}

	/** 根据丄1�7个网络上的图片url，返回一个Bitmap */
	public static Bitmap returnBitMap(String imageName) {
		URL myFileUrl = null;
		Bitmap bitmap = null;
		try {
			myFileUrl = new URL(SERVER_URL + DOWN_FILE_PATH + imageName);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		try {
			HttpURLConnection conn = (HttpURLConnection) myFileUrl
					.openConnection();
			conn.setDoInput(true);
			conn.connect();
			InputStream is = conn.getInputStream();
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bitmap;
	}

	public static void dout(Object obj) {
		if (debugFlag)
			Log.d("[dout]", "obj>>>>>>>>>>>>>" + obj.getClass().getName()
					+ ">>" + obj.toString());
	}

	public static void dout(String str) {
		if (debugFlag)
			Log.d("[dout]", "str>>>>>>>>>>>>>" + str);
	}

	public static void dout(String[] str) {
		if (debugFlag) {
			for (int i = 0; i < str.length; i++)
				Log.d("[dout]", "str[" + i + "]>>>>>>>>>>>>>" + str[i]);
		}
	}

	/**
	 * 去除字符串中的空栄1�7,回车,换行笄1�7,制表笄1�7
	 * */
	public static String rmStrBlank(String str) {
		str = str.trim();
		return str.replaceAll("\\s*|\t|\r|\n", "");
	}

	/**
	 * 设置临时文件的存放地坄1�7
	 */

	public static File getTemporaryFile(Context c) {

		File cacheDir;

		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(android.os.Environment
					.getExternalStorageDirectory(), DOWN_FILE_PATH);
		else
			cacheDir = c.getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();

		return cacheDir;
	}

	
	public static Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);
            
            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale++;
            }
            
            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return null;
    }
}
