package com.adapter;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.activity.R;
import com.tool.ImageLoader;
import com.tool.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoriesAdapter extends BaseAdapter {

	private JSONArray jsonArray;
	private LayoutInflater mInflater;
	private String imageName = null;
	private Activity activity = null;
	private Context context = null;
	
	public CategoriesAdapter(Activity a, Context c, JSONObject ja) throws JSONException {
		this.mInflater = LayoutInflater.from(c);
		this.jsonArray = (JSONArray)ja.get("list");
		this.context = c;
		this.activity = a;
	}
	
	private class RecentViewHolder {
		ImageView ivCategoryImage;
		TextView tvCategoryName;
	}
	
	public int getCount() {
		return jsonArray.length();
	}

	public Object getItem(int arg0) {
		return arg0;
	}

	public long getItemId(int arg0) {
		return arg0;
	}

	public View getView(int position, View convertView, ViewGroup arg2) {
		RecentViewHolder holder = null;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.categorydetaillayout, null);

			holder = new RecentViewHolder();
			holder.ivCategoryImage = (ImageView) convertView
					.findViewById(R.id.category_img);
			holder.tvCategoryName = (TextView) convertView
					.findViewById(R.id.category_name);

			convertView.setTag(holder);
		} else {
			holder = (RecentViewHolder) convertView.getTag();
		}
		
		if(position % 2 > 0){
			convertView.setBackgroundResource(R.drawable.list_bg_selector);
		}else{
			convertView.setBackgroundResource(R.drawable.category_list_bg_selector);
		}
		LoadData(holder,position);
		return convertView;
	}

	/**
	 * 加載數據
	 * 
	 * @author Administrator
	 * 
	 */
	private void LoadData(RecentViewHolder holder, int position) {

		JSONArray jsonobject = null;
		try {
			jsonobject = (JSONArray) jsonArray.get(position);
			holder.tvCategoryName.setText(jsonobject.getString(1));
			imageName = jsonobject.getString(2);

			File f = new File(Utils.getTemporaryFile(context) + "/" +imageName);
			if(!f.exists()){
				ImageLoader imageloader = new ImageLoader(activity,context);
				imageloader.getImage(imageName, holder.ivCategoryImage);
			}else{
				holder.ivCategoryImage.setImageBitmap(Utils.decodeFile(f));
			}
			
		} catch (Exception e) {
			// if something fails do something smart
		}
	}
}
