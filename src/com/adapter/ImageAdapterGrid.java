package com.adapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.activity.R;
import com.tool.ImageLoader;

public class ImageAdapterGrid extends BaseAdapter {

	private Context mContext;
	private JSONArray imageArray;
	private ImageLoader imageLoader;
	private JSONObject jb;
	private ImageView iView;
	private String imageName;

	public ImageAdapterGrid(Activity a , Context c , JSONArray j) {
		this.mContext = c;
		this.imageArray = j;
		imageLoader = new ImageLoader(a , c);
		
	}

	public int getCount() {
		return imageArray.length();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		if(convertView == null){
			iView = new ImageView(mContext);
			iView.setLayoutParams(new GridView.LayoutParams(90, 90));
			iView.setAdjustViewBounds(false);
			iView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			iView.setPadding(8, 8, 8, 8);
		}else{
			iView = (ImageView)convertView;
		}
		
		try {
			jb = imageArray.getJSONObject(position);
			imageName = jb.get("name").toString();
			imageLoader.getImage(imageName , iView);
			iView.setImageResource(R.drawable.stub);
		} catch (JSONException e) {
			e.printStackTrace();
		}
				
		return iView;
	}

}
