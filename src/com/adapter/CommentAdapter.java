package com.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.activity.R;

public class CommentAdapter extends BaseAdapter {

	private JSONArray jsonArray;
	private LayoutInflater mInflater;
	private View convertView_left;
	private View convertView_right;
	private RecentViewHolder holder_left;
	private RecentViewHolder holder_right;
	
	public CommentAdapter(Context c , JSONArray ja) throws JSONException{
		this.mInflater = LayoutInflater.from(c);
		this.jsonArray = ja;
		
		
		
		
	}
	
	private class RecentViewHolder {
		TextView tvTitle;
		TextView tvContent;
		TextView tvTime;
	}
	
	public int getCount() {
		
		return jsonArray.length();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if(position % 2 > 0){
			holder_left = new RecentViewHolder();
			convertView_left = mInflater.inflate(R.layout.commentsdetail_left, null);
			holder_left.tvTitle = (TextView)convertView_left.findViewById(R.id.tv_name);
			holder_left.tvContent = (TextView)convertView_left.findViewById(R.id.tv_content);
			holder_left.tvTime = (TextView)convertView_left.findViewById(R.id.tv_time);
			convertView_left.setTag(holder_left);
			convertView = convertView_left;
			setDate(holder_left,position);
		}else{
			holder_right = new RecentViewHolder();
			convertView_right = mInflater.inflate(R.layout.commentsdetail_right, null);
			holder_right.tvTitle = (TextView)convertView_right.findViewById(R.id.tv_name);
			holder_right.tvContent = (TextView)convertView_right.findViewById(R.id.tv_content);
			holder_right.tvTime = (TextView)convertView_right.findViewById(R.id.tv_time);
			convertView_right.setTag(holder_right);
			convertView = convertView_right;
			setDate(holder_right,position);
		}
		/*
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.commentsdetail_left, null);

			holder = new RecentViewHolder();
			holder.tvTitle = (TextView)convertView.findViewById(R.id.tv_name);
			holder.tvContent = (TextView)convertView.findViewById(R.id.tv_content);
			holder.tvTime = (TextView)convertView.findViewById(R.id.tv_time);

			convertView.setTag(holder);
		} else {
			holder = (RecentViewHolder) convertView.getTag();
		}*/

		
		return convertView;
	}
	
	private void setDate(RecentViewHolder holder,int position){
		try {
			JSONObject jsonobject = jsonArray.getJSONObject(position);
			holder.tvTitle.setText(jsonobject.get("userName").toString() + ":");
			holder.tvContent.setText(jsonobject.get("content").toString());
			holder.tvTime.setText(jsonobject.get("commentTime").toString());
			
			} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
