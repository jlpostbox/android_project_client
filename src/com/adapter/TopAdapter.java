package com.adapter;


import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.activity.R;
import com.tool.ImageLoader;
import com.tool.Utils;

public class TopAdapter extends BaseAdapter {

	private JSONArray jsonArray;
	private LayoutInflater mInflater;
	private String imageName = null;
	private Activity a = null;
	private Context context = null;

	public TopAdapter(Activity a, Context c, JSONObject ja) throws JSONException {
		this.mInflater = LayoutInflater.from(c);
		this.jsonArray = (JSONArray) ja.get("list");
		this.context = c;
		this.a = a;
	}

	private class RecentViewHolder {
		ImageView app_image;
		TextView tvAppname;
		TextView tvAppDescrition;
		TextView tvAppSign;
		TextView tvTopNum;
	}

	public int getCount() {
		return jsonArray.length();
	}

	public Object getItem(int arg0) {
		return arg0;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		RecentViewHolder holder = null;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.topapplistdetail, null);

			holder = new RecentViewHolder();
			holder.app_image = (ImageView) convertView
					.findViewById(R.id.ImageView_image);
			holder.tvAppname = (TextView) convertView
					.findViewById(R.id.TextView_appname);
			holder.tvAppDescrition = (TextView) convertView
					.findViewById(R.id.TextView_appDescrition);
			holder.tvAppSign = (TextView) convertView.findViewById(R.id.TextView_appSign);
			holder.tvTopNum = (TextView) convertView.findViewById(R.id.textView_top);

			convertView.setTag(holder);
		} else {
			holder = (RecentViewHolder) convertView.getTag();
		}

		//填充文字信息
		LoadData(holder, position);

		return convertView;
	}

	/**
	 * 加載數據
	 * 
	 * @author Administrator
	 * 
	 */
	private void LoadData(RecentViewHolder holder, int position) {

		JSONObject jsonobject = null;
		JSONArray signArray = null;
		StringBuffer str_2 = null;

		try {
			jsonobject = jsonArray.getJSONObject(position);
			holder.tvAppname.setText(jsonobject.get("name").toString());
			holder.tvAppDescrition.setText((String) jsonobject
					.get("appDescription"));
			signArray = (JSONArray) jsonobject.get("sign");
			imageName = jsonobject.get("appImg").toString();

			str_2 = new StringBuffer();
			int signArrayLength = signArray.length();
			for (int i = 0; i < signArrayLength; i++) {
				JSONObject s = signArray.getJSONObject(i);
				str_2.append(s.get("name") + " ");
			}
			holder.tvAppSign.setText(str_2);

			File f = new File(Utils.getTemporaryFile(context) + "/" +imageName);
			if(!f.exists()){
				ImageLoader imageloader = new ImageLoader(a,context);
				imageloader.getImage(imageName, holder.app_image);
			}else{
				holder.app_image.setImageBitmap(Utils.decodeFile(f));
			}
			holder.tvTopNum.setText(Integer.toString(position + 1));
			
		} catch (Exception e) {
			// if something fails do something smart
		}
	}
}
