/*
 * Copyright 2011 Sina.
 *
 * Licensed under the Apache License and Weibo License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.open.weibo.com
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sina;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;


import com.sina.AsyncWeiboRunner.RequestListener;


import com.activity.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A dialog activity for sharing any text or image message to weibo.
 * Three parameters , accessToken, tokenSecret, consumer_key, are needed, otherwise a WeiboException 
 * will be throwed.
 * 
 * ShareActivity should implement an interface, RequestListener which will return the request result.
 * 
 * @author  ZhangJie (zhangjie2@staff.sina.com.cn)
 */


public class ShareActivity extends Activity implements OnClickListener, RequestListener{
	private Button mSend;
	private TextView mEdit;
	private FrameLayout mPiclayout;
	
	private String mPicPath = "";
	private String mContent = "";

	
	public static final String EXTRA_WEIBO_CONTENT = "com.weibo.android.content";
	public static final String EXTRA_PIC_URI = "com.weibo.android.pic.uri";
	public static final String EXTRA_ACCESS_TOKEN = "com.weibo.android.accesstoken";
	public static final String EXTRA_TOKEN_SECRET = "com.weibo.android.token.secret";
	
	public static final int WEIBO_MAX_LENGTH = 140;
	
	Weibo mWeibo = Weibo.getInstance();
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.share_mblog_view);
		
		/*Intent in = this.getIntent();
		mPicPath = in.getStringExtra(EXTRA_PIC_URI);
		mContent = in.getStringExtra(EXTRA_WEIBO_CONTENT);
		mAccessToken = in.getStringExtra(EXTRA_ACCESS_TOKEN);
		mTokenSecret = in.getStringExtra(EXTRA_TOKEN_SECRET);
		
		AccessToken accessToken = new AccessToken(mAccessToken, mTokenSecret);
		Weibo weibo = Weibo.getInstance();
		weibo.setAccessToken(accessToken);*/
		
		mPicPath = mWeibo.getmPicPath();
		mContent = mWeibo.getmContent();
		
		if(null == mWeibo.getAccessToken()){
			Uri uri = this.getIntent().getData();
			String oauth_verifier = uri.getQueryParameter("oauth_verifier");
			mWeibo.addOauthverifier(oauth_verifier);
			try {
				mWeibo.generateAccessToken(this, null);
			}catch (WeiboException e1) {
				e1.printStackTrace();
			}
		}
		
		
		Button close = (Button)this.findViewById(R.id.btnClose);
		close.setOnClickListener(this);
		mSend = (Button)this.findViewById(R.id.btnSend);
		mSend.setOnClickListener(this);
		LinearLayout total = (LinearLayout)this.findViewById(R.id.ll_text_limit_unit);
		total.setOnClickListener(this);

		ImageView picture = (ImageView)this.findViewById(R.id.ivDelPic);
		picture.setOnClickListener(this);
		
		mEdit = (TextView)this.findViewById(R.id.tvEdit);
		
		mEdit.setText(mContent);
		mPiclayout = (FrameLayout)ShareActivity.this.findViewById(R.id.flPic);
		if(TextUtils.isEmpty(this.mPicPath)){
			mPiclayout.setVisibility(View.GONE);
		}else{
			mPiclayout.setVisibility(View.VISIBLE);
			File file = new File(mPicPath);
			if(file.exists()){
				Bitmap pic = BitmapFactory.decodeFile(this.mPicPath);
				ImageView image = (ImageView)this.findViewById(R.id.ivImage);
				image.setImageBitmap(pic);
			}else{
				mPiclayout.setVisibility(View.GONE);
			}	
		}
	}

	public void onClick(View v) {
		int viewId = v.getId();
		switch(viewId){
		case R.id.btnClose:
		{
			finish();
			break;
		}
		case R.id.btnSend:
		{
			Weibo weibo = Weibo.getInstance();
			try {
				if(!TextUtils.isEmpty((String)(weibo.getAccessToken().getToken()))){
					if(!TextUtils.isEmpty(mPicPath)){
						upload(weibo, Weibo.APP_KEY, this.mPicPath, this.mContent, "", "");

					}else{
//						Just update a text weibo!
						update(weibo, Weibo.APP_KEY, mContent, "", "");						
					}
				}else{
					Toast.makeText(this, this.getString(R.string.please_login), Toast.LENGTH_LONG);
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (WeiboException e) {
				e.printStackTrace();
			}
			break;
		}
		case R.id.ll_text_limit_unit:
		{
			Dialog dialog = new AlertDialog.Builder(this)
			.setTitle(R.string.attention)
			.setMessage(R.string.delete_all)
			.setPositiveButton(R.string.ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							mEdit.setText("");
						}
					}).setNegativeButton(R.string.cancel, null)
			.create();
			dialog.show();
			break;
		}
		case R.id.ivDelPic:
		{
			Dialog dialog = new AlertDialog.Builder(this)
			.setTitle(R.string.attention)
			.setMessage(R.string.del_pic)
			.setPositiveButton(R.string.ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							mPiclayout.setVisibility(View.GONE);
						}
					}).setNegativeButton(R.string.cancel, null)
			.create();
			dialog.show();
			break;
		}
		default:
		}
	}

	
	private String upload(Weibo weibo, String source, String file, String status, String lon, String lat) 
		throws WeiboException{
			WeiboParameters bundle = new WeiboParameters();
			bundle.add("source", source);
			bundle.add("pic", file);
			bundle.add("status", status);
			if(!TextUtils.isEmpty(lon)){
				bundle.add("lon", lon);
			}
			if(!TextUtils.isEmpty(lat)){
				bundle.add("lat", lat);
			}
			String rlt = "";
			String  url = Weibo.SERVER + "statuses/upload.json";
			AsyncWeiboRunner weiboRunner = new AsyncWeiboRunner(weibo);
			weiboRunner.request(this, url, bundle, Utility.HTTPMETHOD_POST, this);
			
			return rlt;
	}

	private String update(Weibo weibo, String source, String status, String lon, String lat) 
		throws MalformedURLException, IOException, WeiboException{
			WeiboParameters bundle = new WeiboParameters();
			bundle.add("source", source);
			bundle.add("status", status);
			if(!TextUtils.isEmpty(lon)){
				bundle.add("lon", lon);
			}
			if(!TextUtils.isEmpty(lat)){
				bundle.add("lat", lat);
			}
			String rlt = "";
			String url = Weibo.SERVER + "statuses/update.json";
			AsyncWeiboRunner weiboRunner = new AsyncWeiboRunner(weibo);
			weiboRunner.request(this, url, bundle, Utility.HTTPMETHOD_POST, this);
			return rlt;
	}



	public void onComplete(String response) {
		Toast.makeText(this, R.string.send_sucess, Toast.LENGTH_LONG);	
		this.finish();
	}

	public void onIOException(IOException e) {
		// TODO Auto-generated method stub
		
	}

	public void onError(WeiboException e) {
		Toast.makeText(this, String.format(this.getString(R.string.send_failed) + ":%s", e.getMessage()), 
				Toast.LENGTH_LONG);	
	}
	
}
