package com.obj;

import java.util.Date;
import java.util.List;

/**
 * Appinfotable entity. @author MyEclipse Persistence Tools
 */

public class Appinfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String appDescription;
	private String appDownAddress;
	private String appType;
	private String appUseEnvironment;
	private String appSystemRequest;
	private Date appUploadTime = new Date();
	private Integer appView = 0;
	private Integer appComment = 0;
	private String appImg;
	private Integer userId = 1;
	

	private List<Sign> sign;


	public List<Sign> getSign() {
		return sign;
	}

	public void setSign(List<Sign> sign) {
		this.sign = sign;
	}

	// Constructors
	/** default constructor */
	public Appinfo() {
	}

	/** full constructor */
	public Appinfo(String name, String appDescription, String appDownAddress,
			String appType, String appUseEnvironment, String appSystemRequest,
			Date appUploadTime, Integer appView, Integer appComment,
			String appImg, Integer userId) {
		this.name = name;
		this.appDescription = appDescription;
		this.appDownAddress = appDownAddress;
		this.appType = appType;
		this.appUseEnvironment = appUseEnvironment;
		this.appSystemRequest = appSystemRequest;
		this.appUploadTime = appUploadTime;
		this.appView = appView;
		this.appComment = appComment;
		this.appImg = appImg;
		this.userId = userId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAppDescription() {
		return this.appDescription;
	}

	public void setAppDescription(String appDescription) {
		this.appDescription = appDescription;
	}

	public String getAppDownAddress() {
		return this.appDownAddress;
	}

	public void setAppDownAddress(String appDownAddress) {
		this.appDownAddress = appDownAddress;
	}

	public String getAppType() {
		return this.appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppUseEnvironment() {
		return this.appUseEnvironment;
	}

	public void setAppUseEnvironment(String appUseEnvironment) {
		this.appUseEnvironment = appUseEnvironment;
	}

	public String getAppSystemRequest() {
		return this.appSystemRequest;
	}

	public void setAppSystemRequest(String appSystemRequest) {
		this.appSystemRequest = appSystemRequest;
	}

	public Date getAppUploadTime() {
		return this.appUploadTime;
	}

	public void setAppUploadTime(Date appUploadTime) {
		this.appUploadTime = appUploadTime;
	}

	public Integer getAppView() {
		return this.appView;
	}

	public void setAppView(Integer appView) {
		this.appView = appView;
	}

	public Integer getAppComment() {
		return this.appComment;
	}

	public void setAppComment(Integer appComment) {
		this.appComment = appComment;
	}

	public String getAppImg() {
		return this.appImg;
	}

	public void setAppImg(String appImg) {
		this.appImg = appImg;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}