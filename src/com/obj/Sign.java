package com.obj;

/**
 * Signtable entity. @author MyEclipse Persistence Tools
 */

public class Sign implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String signType;  //0:系统标签 1:普通标签

	// Constructors

	/** default constructor */
	public Sign() {
	}

	/** full constructor */
	public Sign(String name, String signType, Integer userId) {
		this.name = name;
		this.signType = signType;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSignType() {
		return this.signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

}