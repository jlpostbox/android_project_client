package com.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;

import com.adapter.AppAdapter;
import com.adapter.CategoriesAdapter;
import com.adapter.TopAdapter;
import com.tool.ServerData;
import com.tool.Utils;

public class MainActivity extends Activity implements OnScrollListener,
		OnClickListener {

	private ListView lv;

	private ServerData serverData;
	private JSONObject jsonObject;
	private JSONObject searchJsonObject;
	private JSONObject topJsonObject;
	private JSONObject categoryJsonObject;
	private JSONObject filterJsonObject;
	private JSONArray jsonObjectList;

	private int currentPage = 1;
	private boolean hasNextPage;
	private AppAdapter appAdapter = null;
	private AppAdapter searchAppAdapter = null;
	private TopAdapter topAppAdapter = null;
	private CategoriesAdapter categoryAdapter = null;
	private EditText etSearch;

	private LinearLayout menuDefault, menuCategory, menuTop, menuSearch;

	private String action = Utils.MENU_DEFAULT;
	private String signId = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.applist);
		findView();
		serverData = new ServerData();

		// 这参数只在“按标签分类”的时候用到
		signId = (String) getIntent().getStringExtra("signId");
		if (signId != null) {
			action = Utils.MENU_FILTERBYSIGNID;
		}
		initialDate(action, 1);
		setListenter();
	}

	private void findView() {
		lv = (ListView) findViewById(R.id.appList);

		menuDefault = (LinearLayout) findViewById(R.id.layout_default);
		menuCategory = (LinearLayout) findViewById(R.id.layout_category);
		menuTop = (LinearLayout) findViewById(R.id.layout_top);
		menuSearch = (LinearLayout) findViewById(R.id.layout_search);

	}

	/**
	 * 设置监听器
	 */
	private void setListenter() {
		lv.setOnItemClickListener(lvListener);
		lv.setOnScrollListener(this);

		menuDefault.setOnClickListener(this);
		menuCategory.setOnClickListener(this);
		menuTop.setOnClickListener(this);
		menuSearch.setOnClickListener(this);

	}

	/**
	 * 初始化数据,这里有多种情况 default:第一次进入 search:搜索功能 top100: top100功能
	 * category:分类功能,后台根据系统分类查询数据 filterBySignId: 按标签分类
	 * 
	 */
	private void initialDate(String action, int requestPage) {
		try {
			if (action == Utils.MENU_DEFAULT) {
				jsonObject = serverData.getData("apps?page=" + requestPage);
				currentPage = Integer.parseInt(jsonObject
						.getString("currentPage"));
				hasNextPage = (jsonObject.getString("hasNextPage") == "true");

				if (requestPage == 1) {
					jsonObjectList = jsonObject.getJSONArray("list");
					appAdapter = new AppAdapter(this, getApplicationContext(),
							jsonObject);
					lv.setAdapter(appAdapter);
				} else {
					int jsonObjectTemList = jsonObject.getJSONArray("list")
							.length();
					for (int i = 0; i < jsonObjectTemList; i++) {
						jsonObjectList.put(jsonObject.getJSONArray("list").get(
								i));
					}
					appAdapter.notifyDataSetChanged();
				}
			} else if (action == Utils.MENU_SEARCH) {
				searchJsonObject = serverData.getData("search?key="
						+ etSearch.getText() + "&page=" + requestPage);
				currentPage = Integer.parseInt(searchJsonObject
						.getString("currentPage"));
				hasNextPage = (searchJsonObject.getString("hasNextPage") == "true");

				if (requestPage == 1) {
					jsonObjectList = searchJsonObject.getJSONArray("list");
					searchAppAdapter = new AppAdapter(this,
							getApplicationContext(), searchJsonObject);
					lv.setAdapter(searchAppAdapter);
				} else {
					int jsonObjectTemList = searchJsonObject.getJSONArray(
							"list").length();
					for (int i = 0; i < jsonObjectTemList; i++) {
						jsonObjectList.put(searchJsonObject
								.getJSONArray("list").get(i));
					}
					searchAppAdapter.notifyDataSetChanged();
				}
			} else if (action == Utils.MENU_TOP) {
				topJsonObject = serverData.getData("top?page=" + requestPage);
				currentPage = Integer.parseInt(topJsonObject
						.getString("currentPage"));
				hasNextPage = (topJsonObject.getString("hasNextPage") == "true");

				if (requestPage == 1) {
					jsonObjectList = topJsonObject.getJSONArray("list");
					topAppAdapter = new TopAdapter(this,
							getApplicationContext(), topJsonObject);
					lv.setAdapter(topAppAdapter);
				} else {
					int jsonObjectTemList = topJsonObject.getJSONArray("list")
							.length();
					for (int i = 0; i < jsonObjectTemList; i++) {
						jsonObjectList.put(topJsonObject.getJSONArray("list")
								.get(i));
					}
					topAppAdapter.notifyDataSetChanged();
				}
			} else if (action == Utils.MENU_CATEGORY) {
				categoryJsonObject = serverData
						.getData("getSystemCategories?page=" + requestPage);
				currentPage = Integer.parseInt(categoryJsonObject
						.getString("currentPage"));
				hasNextPage = (categoryJsonObject.getString("hasNextPage") == "true");

				if (requestPage == 1) {
					jsonObjectList = categoryJsonObject.getJSONArray("list");
					categoryAdapter = new CategoriesAdapter(this,
							getApplicationContext(), categoryJsonObject);
					lv.setAdapter(categoryAdapter);
				} else {
					int jsonObjectTemList = categoryJsonObject.getJSONArray(
							"list").length();
					for (int i = 0; i < jsonObjectTemList; i++) {
						jsonObjectList.put(categoryJsonObject.getJSONArray(
								"list").get(i));
					}
					categoryAdapter.notifyDataSetChanged();
				}
			} else if (action == Utils.MENU_FILTERBYSIGNID) {
				filterJsonObject = serverData.getData("appsBySign?signId="
						+ signId + "&page=" + requestPage);
				currentPage = Integer.parseInt(filterJsonObject
						.getString("currentPage"));
				hasNextPage = (filterJsonObject.getString("hasNextPage") == "true");

				if (requestPage == 1) {
					jsonObjectList = filterJsonObject.getJSONArray("list");
					appAdapter = new AppAdapter(this, getApplicationContext(),
							filterJsonObject);
					lv.setAdapter(appAdapter);
				} else {
					int jsonObjectTemList = filterJsonObject.getJSONArray(
							"list").length();
					for (int i = 0; i < jsonObjectTemList; i++) {
						jsonObjectList.put(filterJsonObject
								.getJSONArray("list").get(i));
					}
					appAdapter.notifyDataSetChanged();
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private ListView.OnItemClickListener lvListener = new ListView.OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			// 增加浏览量
			try {
				serverData.getData("updateAppView?appID="
						+ (Integer) ((JSONObject) jsonObjectList.get(position))
								.get("id"));
			} catch (JSONException e1) {
				e1.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			// 查询分类全部应用
			if (action == Utils.MENU_CATEGORY) {

				try {
					signId = ((JSONArray) (jsonObjectList.get(position)))
							.getString(0);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				action = Utils.MENU_FILTERBYSIGNID;
				initialDate(action, 1);
			} else {
				// 应用的详细信息
				Intent intent = new Intent(getApplicationContext(),
						AppDetail.class);
				Bundle bundle = new Bundle();
				try {
					bundle.putString("jsonObject", jsonObjectList
							.getString(position));
					bundle.putSerializable("serverDataObject", serverData);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				intent.putExtras(bundle);
				startActivity(intent);
			}
		}

	};

	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// 当不滚动时
		if (scrollState == SCROLL_STATE_IDLE) {
			// 判断滚动到底部
			if (view.getLastVisiblePosition() == (view.getCount() - 1)
					&& hasNextPage) {
				initialDate(action, currentPage + 1);

			}
		}

	}

	// 底部菜单响应事件
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.layout_default:
			action = Utils.MENU_DEFAULT;
			initialDate(action, 1);
			break;
		case R.id.layout_category:
			action = Utils.MENU_CATEGORY;
			initialDate(action, 1);
			break;
		case R.id.layout_top:
			action = Utils.MENU_TOP;
			initialDate(action, 1);
			break;
		case R.id.layout_search:
			searchDialog();
			break;
		default:
			Toast.makeText(MainActivity.this, "nothing", Toast.LENGTH_SHORT)
					.show();
		}
	}

	// 搜索功能
	private void searchDialog() {
		LayoutInflater factory = LayoutInflater.from(this);
		final View textEntryView = factory.inflate(R.layout.searchform, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setIcon(R.drawable.icon_search);
		builder.setTitle(R.string.search_form);
		builder.setView(textEntryView);
		builder.setPositiveButton(R.string.search_form,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						etSearch = (EditText) textEntryView
								.findViewById(R.id.etSearch);
						try {
							action = Utils.MENU_SEARCH;
							initialDate(action, 1);
							dialog.dismiss();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).setNegativeButton(R.string.cancel_form,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						/* User clicked cancel so do some stuff */
					}
				});
		builder.show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			new AlertDialog.Builder(this).setIcon(
					android.R.drawable.ic_dialog_info).setTitle("Exit Now?")
					.setNegativeButton(R.string.cancel_form,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
								}
							}).setPositiveButton(R.string.sumbit_form,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									finish();
								}
							}).show();

			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}

	}

}