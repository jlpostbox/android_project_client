package com.activity;

import java.io.File;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TabHost.OnTabChangeListener;

import com.adapter.CommentAdapter;
import com.adapter.ImageAdapterGrid;
import com.facebook.BaseDialogListener;
import com.facebook.BaseRequestListener;
import com.facebook.SessionEvents;
import com.facebook.SessionStore;
import com.facebook.SessionEvents.AuthListener;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.facebook.android.Facebook.DialogListener;
import com.sina.RequestToken;
import com.sina.ShareActivity;
import com.sina.Weibo;
import com.sina.WeiboException;
import com.tool.ServerData;
import com.tool.Utils;


public class AppDetail extends TabActivity implements OnTabChangeListener,OnScrollListener {

	private static final String TAG = "AppDetail";
	private TabHost myTabhost;
	private ServerData serverData;
	JSONObject app = null;
	JSONObject commentsObject;
	JSONObject commentsDetail;
	JSONObject jsonObject;

	ImageView app_image;
	TextView tvAppname;
	TextView tvAppDescrition;
	LinearLayout linearlayout;
	LinearLayout menuPreviousPage, menuAddComment, menuNextPage;

	ListView lv;
	EditText etUserName;
	EditText etContent;
	Button btPostComment;
	Button btAddComment;
	
	ImageButton ib_facebook;
	ImageButton ib_sina;
	ImageButton ib_twitter;

	JSONArray imageArray;

	int appId;

	private int currentPage = 1;
	
	private Facebook mFacebook;
    private AsyncFacebookRunner mAsyncRunner;
    public static final String APP_ID = "110377099061013";
    private static final String[] PERMISSIONS =
        new String[] {"publish_stream", "read_stream", "offline_access","email"};
    
    //------sina---------
    private static final String URL_ACTIVITY_CALLBACK = "weiboandroidsdk://TimeLineActivity";
	private static final String FROM = "xweibo";
	
	private static final String CONSUMER_KEY = "3811434321";
	private static final String CONSUMER_SECRET = "ee1168b97cb7093e752a236adc9b29b3";
	//------end---------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		try {
			app = new JSONObject(getIntent().getStringExtra("jsonObject"));
			serverData = (ServerData) getIntent().getSerializableExtra(
					"serverDataObject");
			init();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		mFacebook = new Facebook(APP_ID);
       	mAsyncRunner = new AsyncFacebookRunner(mFacebook);

        SessionStore.restore(mFacebook, this);
        SessionEvents.addAuthListener(new SampleAuthListener());
	}

	public class SampleAuthListener implements AuthListener {

        public void onAuthSucceed() {
            Toast.makeText(AppDetail.this, "You have logged in!", Toast.LENGTH_SHORT).show();
        }

        public void onAuthFail(String error) {
            Toast.makeText(AppDetail.this, "Login Failed: " + error, Toast.LENGTH_SHORT).show();
        }
    }
	
	private void init() throws JSONException {
		setTab();
		setApp();
	}

	/**
	 * 切换 Tab 时触发的事件
	 */
	public void onTabChanged(String arg0) {

		if (arg0.equals("ApplicationID")) {
		} else if (arg0.equals("CommentsID")) {
			try {
				getComments(currentPage);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				getImages();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 为不同的 Tab 设置不同的菜单
	 *//*
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		MenuInflater inflater = getMenuInflater();
		switch (myTabhost.getCurrentTab()) {
		case 0:
			// inflater.inflate(tabMenus[0], menu);
			break;
		case 1:
			inflater.inflate(R.menu.comment_menu, menu);
			break;
		case 2:
			// inflater.inflate(tabMenus[2], menu);
			break;
		default:
			break;
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/*	*//**
	 * 用户按手机 menu 键时触发事件 检查菜单菜单是否可用
	 *//*
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {

		try {
			if (commentsObject.getString("hasPreviousPage") == "false") {
				menu.getItem(0).setEnabled(false);
			} else {
				menu.getItem(0).setEnabled(true);
			}

			if (commentsObject.getString("hasNextPage") == "false") {
				menu.getItem(2).setEnabled(false);
			} else {
				menu.getItem(2).setEnabled(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return super.onMenuOpened(featureId, menu);
	}

	*//**
	 * 菜单响应事件
	 *//*
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.previous_page:
			try {
				if (commentsObject.getString("hasPreviousPage") != null
						&& commentsObject.getString("hasPreviousPage") == "true") {
					getComments(Integer.parseInt(commentsObject
							.getString("currentPage")) - 1);
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		case R.id.addComment:
			showAddCommentDialog();
			return true;
		case R.id.next_page:
			try {
				if (commentsObject.getString("hasNextPage") != null
						&& commentsObject.getString("hasNextPage") == "true") {
					getComments(Integer.parseInt(commentsObject
							.getString("currentPage")) + 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}*/

	/**
	 * 添加 Comment 的 Dialog
	 * 
	 * @return
	 */
	private void showAddCommentDialog() {

		LayoutInflater factory = LayoutInflater.from(this);
		final View textEntryView = factory.inflate(R.layout.addcommentform,
				null);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setIcon(R.drawable.icon_add_comment).setTitle(
				R.string.comment_title).setView(textEntryView)
				.setPositiveButton(R.string.sumbit_form,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								etUserName = (EditText) textEntryView
										.findViewById(R.id.EditText_user_name);
								etContent = (EditText) textEntryView
										.findViewById(R.id.EditText_content);
								try {
									serverData
											.getDataString("addcomment?appId="
													+ appId
													+ "&userName="
													+ URLEncoder
															.encode(
																	etUserName
																			.getText()
																			.toString(),
																	"utf-8")
													+ "&content="
													+ URLEncoder
															.encode(
																	etContent
																			.getText()
																			.toString(),
																	"utf-8"));
									dialog.dismiss();
									getComments(currentPage);
								} catch (JSONException e) {
									e.printStackTrace();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}).setNegativeButton(R.string.cancel_form,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								/* User clicked cancel so do some stuff */
							}
						});
		builder.show();
	}

	/**
	 * 定义标签页
	 */
	private void setTab() {
		myTabhost = this.getTabHost();
		// get Tabhost
		LayoutInflater.from(this).inflate(R.layout.appdetail,
				myTabhost.getTabContentView(), true);
		// myTabhost.setBackgroundColor(Color.argb(150, 22, 70, 150));

		myTabhost
				.addTab(myTabhost.newTabSpec("ApplicationID")
						// make a new Tab
						.setIndicator(
								"Application",
								getResources().getDrawable(
										R.drawable.icon_application))
						.setContent(R.id.RelativeLayout_app));

		myTabhost.addTab(myTabhost.newTabSpec("CommentsID")
				// make a new Tab
				.setIndicator("Comments",
						getResources().getDrawable(R.drawable.icon_comment))
				.setContent(R.id.LinearLayout_comments));

		myTabhost.addTab(myTabhost.newTabSpec("ImagesID")
				// make a new Tab
				.setIndicator("Images",
						getResources().getDrawable(R.drawable.icon_image))
				.setContent(R.id.GridView_image));
		myTabhost.setOnTabChangedListener(this);
	}

	/**
	 * 所有应用软件
	 * 
	 * @throws JSONException
	 */
	String imageName;
	private void setApp() throws JSONException {

		app_image = (ImageView) findViewById(R.id.ImageView_image);
		tvAppname = (TextView) findViewById(R.id.TextView_appname);
		tvAppDescrition = (TextView) findViewById(R.id.TextView_appDescrition);
		linearlayout = (LinearLayout) findViewById(R.id.LinearLayout_sign);

		tvAppname.setText(app.get("name").toString());
		tvAppDescrition.setText((String) app.get("appDescription"));
		
		ib_facebook = (ImageButton)findViewById(R.id.ib_facebook);
		ib_facebook.setOnClickListener(new ButtonOnClickListener());
		ib_sina = (ImageButton)findViewById(R.id.ib_sina);
		ib_sina.setOnClickListener(new ButtonOnClickListener());
		ib_twitter = (ImageButton)findViewById(R.id.ib_twitter);
		ib_twitter.setOnClickListener(new ButtonOnClickListener());
		
		JSONArray signArray = (JSONArray) app.get("sign");

		imageName = app.get("appImg").toString();
		File f = new File(Utils.getTemporaryFile(this) + "/" + imageName);
		if (f.exists()) {
			app_image.setImageBitmap(Utils.decodeFile(f));
		} else {
			Bitmap bm = Utils.returnBitMap(imageName);
			if (bm != null) {
				app_image.setImageBitmap(bm);
			}
		}

		int signLength = signArray.length();
		for (int i = 0; i < signLength; i++) {
			JSONObject s = signArray.getJSONObject(i);
			final String signId = s.get("id").toString();
			TextView tvSign = new TextView(this);
			tvSign.setText(Html.fromHtml("<u>" + s.get("name").toString()
					+ "</u>"));
			tvSign.setPadding(0, 0, 10, 0);
			tvSign.setTextColor(this.getResources().getColorStateList(
					R.drawable.mysign));

			tvSign.setOnClickListener(new OnClickListener() {

				public void onClick(View arg0) {

					Intent intent = new Intent(getApplicationContext(),MainActivity.class);
					Bundle bundle = new Bundle();
					bundle.putString("signId", signId);
					intent.putExtras(bundle);
					startActivity(intent);
				}

			});
			linearlayout.addView(tvSign);
		}


	}
	
	/**
	 * sina,facebook,twitter的监听事件
	 * @author leohe
	 *
	 */
	private final class ButtonOnClickListener implements OnClickListener {
        
        public void onClick(View arg0) {
                        
            switch (arg0.getId()) {
    		case R.id.ib_sina:
    			    			
    			Weibo weibo = Weibo.getInstance();
    			weibo.setmContent(tvAppname.getText().toString());
    			
    			File file = Environment.getExternalStorageDirectory();
				String sdPath = file.getAbsolutePath();
				String picPath = sdPath + "/UploadImages/" + imageName;
				//System.out.println("图片地址：＝＝＝＝＝＝＝＝" + picPath);
    			weibo.setmPicPath(picPath);
    			
    			if(null == weibo.getAccessToken()){
					weibo.setupConsumerConfig(CONSUMER_KEY, CONSUMER_SECRET);
					try {					
						RequestToken requestToken = weibo.getRequestToken(AppDetail.this, Weibo.APP_KEY, 
								Weibo.APP_SECRET, URL_ACTIVITY_CALLBACK);
						
						Uri uri = Uri.parse(Weibo.URL_AUTHENTICATION + "?display=wap2.0&oauth_token=" + 
								requestToken.getToken() + "&from=" + FROM);
						startActivity(new Intent(Intent.ACTION_VIEW, uri));
					}catch (WeiboException e){
						e.printStackTrace();
					}
    			}else{
    				startActivity(new Intent(getApplicationContext(), ShareActivity.class));
    			}
				
    			break;
    		case R.id.ib_facebook:
    			    			
    			if (mFacebook.isSessionValid()) {
                    //SessionEvents.onLogoutBegin();
                    //AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFacebook);
                    shareToFacebook();
                    //asyncRunner.logout(getContext(), new LogoutRequestListener());
                } else {
                	mFacebook.authorize(AppDetail.this, PERMISSIONS,new LoginDialogListener());
                }
    			
    			break;
    		case R.id.ib_twitter:
    			Toast.makeText(AppDetail.this, "twitter", Toast.LENGTH_SHORT).show();
    			break;
    		default:
    			Toast.makeText(AppDetail.this, "nothing", Toast.LENGTH_SHORT).show();
    		}
        }
    }
	
	/**
	 * facebook
	 * @author leohe
	 *
	 */
	private void shareToFacebook(){
		
		Bundle params = new Bundle();
        params.putString("picture", "http://www.google.com.hk/intl/zh-CN/images/logo_cn.png");
        
        params.putString("description", "DianpingAndroid Application");
        
        params.putString("name", tvAppname.getText().toString());
		//params.putString("link", "http://www.imore.hk/upload/snap/2_3/11/05/3952.jpg");
        
    	// 发送消息 对话框
        mFacebook.dialog(AppDetail.this, "feed",params, new SampleDialogListener());
	}

	/**
	 * facebook
	 * @author leohe
	 *
	 */
	public class SampleDialogListener extends BaseDialogListener {

        public void onComplete(Bundle values) {
            final String postId = values.getString("post_id");
            if (postId != null) {
                Log.d(TAG, "Dialog Success! post_id=" + postId);
                
                mAsyncRunner.request(postId, new WallPostRequestListener());
            } else {
                Log.d(TAG, "No wall post made");
            }
        }
    }
	
	/**
	 * facebook
	 * @author leohe
	 *
	 */
	public class WallPostRequestListener extends BaseRequestListener {

        public void onComplete(final String response, final Object state) {
            Log.d(TAG, "Got response: " + response);
            String message = "<empty>";
            try {
                JSONObject json = Util.parseJson(response);
                // 获取发布在信息墙的消息是什么内容：
                message = json.getString("message");
            } catch (JSONException e) {
                Log.w(TAG, "JSON Error in response");
            } catch (FacebookError e) {
                Log.w(TAG, "Facebook Error: " + e.getMessage());
            }
            
            
            final String text = "Your Wall Post: " + message;
            AppDetail.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(AppDetail.this, text, Toast.LENGTH_SHORT).show();
                }
            });
        }

		public void onFacebookError(FacebookError arg0, Object arg1) {
			// TODO Auto-generated method stub
			
		}
    }
	
	/**
	 * facebook
	 * @author leohe
	 *
	 */
    private final class LoginDialogListener implements DialogListener {
        public void onComplete(Bundle values) {
            SessionEvents.onLoginSuccess();
            shareToFacebook();
        }
        public void onFacebookError(FacebookError error) {
            SessionEvents.onLoginError(error.getMessage());
        }
        
        public void onError(DialogError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onCancel() {
            SessionEvents.onLoginError("Action Canceled");
        }
    }

	/**
	 * 所有评论
	 * 
	 * @param currentPage
	 * @throws Exception
	 */
	private void getComments(int currentPage) throws Exception {

		appId = Integer.parseInt(app.get("id").toString());

		commentsObject = serverData.getData("comments?page=" + currentPage
				+ "&appId=" + appId);
		
		
		menuPreviousPage = (LinearLayout) findViewById(R.id.layout_previous_page);
		menuAddComment = (LinearLayout) findViewById(R.id.layout_add_comment);
		menuNextPage = (LinearLayout) findViewById(R.id.layout_next_page);
		menuAddComment.setOnClickListener(addCommentListener);
		menuPreviousPage.setOnClickListener(previousPageListener);
		menuNextPage.setOnClickListener(nextPageListener);
		
		
		// 判斷是否有數據
		JSONArray jsonArray = (JSONArray) commentsObject.get("list");
		if (jsonArray.length() > 0) {
			lv = (ListView) findViewById(R.id.commentList);
			lv.setAdapter(new CommentAdapter(getApplicationContext(),jsonArray));

		}
	}

	private View.OnClickListener previousPageListener = new View.OnClickListener() {

		public void onClick(View arg0) {

			try {
				if (commentsObject.getString("hasPreviousPage") != null
						&& commentsObject.getString("hasPreviousPage") == "true") {
					getComments(Integer.parseInt(commentsObject
							.getString("currentPage")) - 1);
				} else {
					Toast.makeText(AppDetail.this, "It is the first page!", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	};

	private View.OnClickListener addCommentListener = new View.OnClickListener() {

		public void onClick(View arg0) {

			showAddCommentDialog();
		}
	};

	private View.OnClickListener nextPageListener = new View.OnClickListener() {

		public void onClick(View arg0) {

			try {
				if (commentsObject.getString("hasNextPage") != null
						&& commentsObject.getString("hasNextPage") == "true") {
					getComments(Integer.parseInt(commentsObject
							.getString("currentPage")) + 1);
				}else{
					Toast.makeText(AppDetail.this, "It is the last page!", Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * 所有图片
	 * 
	 * @throws Exception
	 */
	private void getImages() throws Exception {

		final int appId = Integer.parseInt(app.get("id").toString());

		if (imageArray == null) {
			imageArray = new JSONArray(serverData.getDataString("images?appId="
					+ appId));
		}
		if (imageArray.length() > 0) {
			ImageAdapterGrid imageAdapterGrid = new ImageAdapterGrid(this,
					getApplicationContext(), imageArray);
			GridView gView = (GridView) this.findViewById(R.id.GridView_image);
			gView.setAdapter(imageAdapterGrid);// 添加元素
			gView.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {

					Intent intent = new Intent(getApplicationContext(),
							ImageSwitcher1.class);
					Bundle bundle = new Bundle();
					bundle.putStringArray("imageArray", setImage());
					bundle.putInt("imageIndex", arg2);
					intent.putExtras(bundle);
					startActivity(intent);
				}

			});
		}
	}

	private String[] setImage() {

		String[] mImageIds = new String[imageArray.length()];
		int imageArrayLength = imageArray.length();
		for (int i = 0; i < imageArrayLength; i++) {
			try {
				mImageIds[i] = imageArray.getJSONObject(i).get("name")
						.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return mImageIds;
	}

	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		//当不滚动时  
		if(scrollState == SCROLL_STATE_IDLE){  
			System.out.println(view.getFirstVisiblePosition()+"===" + view.getLastVisiblePosition()+"==="+view.getCount());
			//判断滚动到底部   
			if(view.getLastVisiblePosition()==(view.getCount()-1)){  
				Toast.makeText(AppDetail.this, "到底啦！", Toast.LENGTH_SHORT).show();
			}
		}
	}

}
