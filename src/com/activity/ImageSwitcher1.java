package com.activity;

import java.io.File;

import com.tool.Utils;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Gallery.LayoutParams;

public class ImageSwitcher1 extends Activity implements AdapterView.OnItemSelectedListener {

	private Gallery g_big;


	private int index;
	private File cacheDir;

	
	public int g_position = 0;


	File f = null;
	String filename = null;

	private String[] mImageIds;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.image_switcher_1);

		cacheDir = Utils.getTemporaryFile(getApplicationContext());
		System.out.println("=====================================>>" + cacheDir);
        
		mImageIds = getIntent().getStringArrayExtra("imageArray");
		index = getIntent().getIntExtra("imageIndex", 0);

		g_big = (Gallery) findViewById(R.id.gallery);
		g_big.setAdapter(new ImageAdapter(this));
		g_big.setSelection(index);
		g_big.setOnItemSelectedListener(this);

	}

	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {


	}
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}


	public class ImageAdapter extends BaseAdapter {

		private Context mContext;
		//private int[] myImageIds = {R.drawable.g1,R.drawable.g2,R.drawable.g3,R.drawable.g4};

		public ImageAdapter(Context c) {
			mContext = c;
			//imageLoader = new ImageLoader(getParent() , c);
		}

		public int getCount() {
			return mImageIds.length;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView i = new ImageView(mContext);
			
			//i.setImageResource(myImageIds[position]);

			String filename=mImageIds[position];
	        File f=new File(cacheDir, filename);
	        
	        System.out.println("File Name =====================================>>" + Uri.fromFile(f));
	        
	        i.setImageURI(Uri.fromFile(f));
			i.setAdjustViewBounds(true);
			i.setLayoutParams(new Gallery.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			//i.setBackgroundResource(R.drawable.picture_frame);

			return i;
		}

	}


	

}
